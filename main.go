package main

import (
	"github.com/emicklei/go-restful"
	"log"
	"net/http"
)

func main() {
	// 创建一个新的WebService
	ws := new(restful.WebService)

	// 添加一个路由
	ws.Route(ws.GET("/hello").To(hello))

	// 将WebService添加到Go-RESTful框架中
	restful.Add(ws)

	// 启动HTTP服务器
	log.Fatal(http.ListenAndServe(":80", nil))
}

func hello(request *restful.Request, response *restful.Response) {
	response.Write([]byte("Hello, world!"))
}
